<?php

class Medicamento {
    /**
     * @var PDO
     */
    private $conn;
    private $table_name = "medicamentos";

    // Object properties
    public $id_p;
    public $id_e;
    public $latitud;
    public $longitud;
    public $medicamento;
    public $presentacion;
    public $monto_empaque;
    public $condicion_v;
    public $estab;
    public $direcccion;
    public $ubicacion;
    public $telefono;
    public $horario;


    // Constructor with $db as database connection
    public function  __construct($db) {
        $this -> conn = $db;
    }

    // Read all measures
    function read_all() {
        $query = "SELECT * FROM " . $this->table_name;
        $stmt = $this -> conn -> prepare($query);
        $stmt -> execute();
        return $stmt;
    }

    function search($search) {
        $query = "SELECT id, medicamento, presentacion, direccion, monto_empaque, estab, latitud, longitud FROM " . $this -> table_name . " WHERE  medicamento LIKE '%". $search ."%' ORDER BY monto_empaque ASC";
        $stmt = $this -> conn -> prepare($query);
        $stmt -> execute();
        return $stmt;
    }

    function search_details($id) {
        $query = "SELECT * FROM " . $this -> table_name . " WHERE id=$id LIMIT 1";
        $stmt = $this -> conn -> prepare($query);
        $stmt -> execute();
        return $stmt;
    }

    function create() {
        if ($this -> id_p == null || $this -> id_e == null ||  $this -> latitud == null || $this -> longitud == null ||$this -> medicamento == null || $this -> presentacion == null || $this -> presentacion == null || $this -> monto_empaque == null || $this -> condicion_v == null || $this -> estab == null || $this -> direcccion == null || $this -> ubicacion == null || $this -> telefono == null || $this -> horario == null) {
            return false;
        }

        $query = "INSERT INTO " . $this -> table_name . " SET id_p=:id_p, id_e=:id_e, latitud=:latitud, longitud=:longitud, medicamento=:medicamento, presentacion=:presentacion, monto_empaque=:monto_empaque, condicion_v=:condicion_v, estab=:estab, direccion=:direccion, ubicacion=:ubicacion, telefono=:telefono, horario=:horario";

        $stmt = $this -> conn -> prepare($query);

        $stmt -> bindParam(":id_p", $this -> id_p);
        $stmt -> bindParam(":id_e", $this -> id_e);
        $stmt -> bindParam(":latitud", $this -> latitud);
        $stmt -> bindParam(":longitud", $this -> longitud);
        $stmt -> bindParam(":medicamento", $this -> medicamento);
        $stmt -> bindParam(":presentacion", $this -> presentacion);
        $stmt -> bindParam(":monto_empaque", $this -> monto_empaque);
        $stmt -> bindParam(":condicion_v", $this -> condicion_v);
        $stmt -> bindParam(":estab", $this -> estab);
        $stmt -> bindParam(":direccion", $this -> direcccion);
        $stmt -> bindParam(":ubicacion", $this -> ubicacion);
        $stmt -> bindParam(":telefono", $this -> telefono);
        $stmt -> bindParam(":horario", $this -> horario);

        if ($stmt -> execute()) {
            return true;
        }
        return false;

    }
}

?>