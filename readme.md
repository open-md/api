# OpenMD RESTful API
This is the API of OpenMD, a free data project for medicine made by UTEC students.

## Columns of the table
* ```id_p [int(50)]```: Id given by the Health Department to identify the medicine
* ```id_e [int(100)]```: Id of the establishment where the med is located
* ```medicamento [varchar(100)]```: Name of the med.
* ```presentacion [varchar(100)]```: Short description
* ```monto_empaque [decimal(10,2)]```: Price
* ```condicion_v [varchar(100)]```: The conditions that must be met to be able to buy the med
* ```estab [varchar(100)]```: Establishment where you can buy the med
* ```latitud [decimal(20,10)]```: Latitude
* ```longitud [decimal(20,10)]```: Longitude
* ```direccion [varchar(100)]```: Location of the establishment
* ```ubicacion [varchar(100)]```: More info about the location
* ```telefono [varchar(100)]```: Phone number of the location
* ```horario [varchar(100)]```: Schedule of the location

## How to make a request:
The API has two methods:
1. To add a new meds record. It's done by a POST method.
2. To read all the records from the table

**Notice that you can't send an empty value. If not sure, send "Null"**

All the requests must have this header:
```
Content-Type: application/json
```

### Adding a new record
POST request to ```http://innovations.pe/arturo/api-meds/medicamentos/create/```

The body of the request must be a raw JSON string like this:
```
{
	"id_p": 3,
	"id_e": 34,
	"medicamento": "Paracetamol 100mg",
	...
}
```

### Getting records
#### All the records
GET request to ```http://innovations.pe/arturo/api-meds/medicamentos/read_all/```

#### Search by name
POST request to ```http://innovations.pe/arturo/api-meds/medicamentos/search/```

The body of the request must be a raw JSON string like this:
```
{
	"search": "name-of-the-med"
}
```

This will give you as an output:
```
{
	"records": [
		{
		    "id": ...,
		    "medicamento": ...,
		    "presentacion": ...,
		    "monto_empaque": ...,
		    "estab": ...,
		    "direccion": ...,
		    "latitud": ...,
		    "longitud": ...
		},
		{
		    ...
		},
		...
	]
}
```
With the ```id``` from this request, you can make an even more detailed search with a POST request to ```http://innovations.pe/arturo/api-meds/medicamentos/search_details/```

The body of the request must be a raw JSON string like this:
```
{
	"id": "my-id"
}
```

