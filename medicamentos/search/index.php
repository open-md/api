<?php
// Required headers
header("Access-Control-Allow-Origin: *"); // Specifies which origin can access the resources. * means all
header("Content-Type: application/json; charset=UTF-8");

// Include database and object files
include_once '../../config/Database.php';
include_once '../../objects/Medicamento.php';

// Instantiate database and product object
$database = new Database();
$db = $database -> getConnection();

// Get posted data
$data = json_decode(file_get_contents("php://input"), true);

// Initialize product
$med = new Medicamento($db);

// Query products
$stmt = $med -> search($data["search"]);
$num = $stmt -> rowCount();

if ($data["search"]) {
    // Check if more than 0 records were found
    if ($num > 0) {
        // Products array
        $med_arr = array();
//        $med_arr["records"] = array();

        // Retrieve table contents
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            extract($row);

            if (!array_key_exists($direccion,$med_arr)) {
                $med_arr[$direccion] = array();
                $med_arr[$direccion]["estab"] = $estab;
                $med_arr[$direccion]["latitud"] =  $latitud;
                $med_arr[$direccion]["longitud"] = $longitud;
                $med_arr[$direccion]["contenido"] = array();
            }

            $med_content = array(
                "id" => $id,
                "medicamento" => $medicamento,
                "presentacion" => $presentacion,
                "monto_empaque" => $monto_empaque
            );

            array_push($med_arr[$direccion]["contenido"],$med_content);
//            array_push($med_arr, $med_item);
        }

        echo json_encode($med_arr);
    } else {
        echo json_encode(
            array("message" => "No se encontraron medicamentos")
        );
    }
} else {
    echo json_encode(
        array("message" => "No se especificó un parámetro de búsqueda")
    );
}
