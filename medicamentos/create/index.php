<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With");

// Get database connection
include_once '../../config/Database.php';
include_once '../../objects/Medicamento.php';

$database = new Database();
$db = $database -> getConnection();

$med = new Medicamento($db);

// Get posted data
$data = json_decode(file_get_contents("php://input"), true);

// Set measure values
$med -> id_p = $data["id_p"];
$med -> id_e = $data["id_e"];
$med -> latitud = $data["latitud"];
$med -> longitud = $data["longitud"];
$med -> medicamento = $data["medicamento"];
$med -> presentacion = $data["presentacion"];
$med -> monto_empaque = $data["monto_empaque"];
$med -> condicion_v = $data["condicion_v"];
$med -> estab = $data["estab"];
$med -> direcccion = $data["direccion"];
$med -> ubicacion = $data["ubicacion"];
$med -> telefono = $data["telefono"];
$med -> horario = $data["horario"];

if ($med -> create()) {
    echo json_encode(
        array("message" => "Medicamento añadido")
    );
} else {
    echo json_encode(
        array("message" => "Error al añadir el medicamento")
    );
}

?>