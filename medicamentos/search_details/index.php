<?php
// Required headers
header("Access-Control-Allow-Origin: *"); // Specifies which origin can access the resources. * means all
header("Content-Type: application/json; charset=UTF-8");

// Include database and object files
include_once '../../config/Database.php';
include_once '../../objects/Medicamento.php';

// Instantiate database and product object
$database = new Database();
$db = $database -> getConnection();

// Initialize product
$med = new Medicamento($db);
// Get posted data
$data = json_decode(file_get_contents("php://input"), true);

if($data["id"]) {
// Query products
    $stmt = $med -> search_details($data["id"]);
    $num = $stmt -> rowCount();

    // Check if more than 0 records were found
    if ($num > 0) {

        // Retrieve table contents
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            // Extract row. This will make $row['name'] into just $name
            extract($row);

            $med_item = array(
                "id_p" => $id_p,
                "id_e" => $id_e,
                "latitud" => $latitud,
                "longitud" => $longitud,
                "medicamento" => $medicamento,
                "presentacion" => $presentacion,
                "monto_empaque" => $monto_empaque,
                "condicion_v" => $condicion_v,
                "estab" => $estab,
                "direccion" => $direccion,
                "ubicacion" => $ubicacion,
                "telefono" => $telefono,
                "horario" => $horario,
            );
        }

        echo json_encode($med_item);
    } else {
        echo json_encode(
            array("message" => "No se encontraron medicamentos")
        );
    }
} else {
    echo json_encode(
        array("message" => "No se especificó un parámetro de búsqueda")
    );
}
